///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file animal.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Joshua Lorica <loricaj@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   3/23/21
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>

#include "animal.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"

using namespace std;

namespace animalfarm {
	
void Animal::printInfo() {
	cout << "   Species = [" << species << "]" << endl;
	cout << "   Gender = [" << genderName( gender ) << "]" << endl;
}

Animal::Animal(void) { cout << '.'; };
Animal::~Animal(void) { cout << 'x'; };


string Animal::genderName (enum Gender gender) {
   switch (gender) {
      case MALE:    return string("Male"); break;
      case FEMALE:  return string("Female"); break;
      case UNKNOWN: return string("Unknown"); break;
   }

   return string("Really, really Unknown");
};
	

string Animal::colorName (enum Color color) {
   switch (color) {
      case BLACK  : return string("Black");  break;
      case WHITE  : return string("White");  break;
      case RED    : return string("Red");    break;
      case SILVER : return string("Silver"); break;
      case YELLOW : return string("Yellow"); break;
      case BROWN  : return string("Brown");  break;
   }

   return string("Unknown");
};

const enum Gender Animal::getRandomGender(){
   Gender randomGender = Gender(rand()%3);
   return randomGender;
}

const enum Color Animal::getRandomColor(){
   Color randomColor = Color(rand()%6);
   return randomColor;
}

const bool Animal::getRandomBool(){
   bool randomBool = rand()%2;
   return randomBool;
}

const float Animal::getRandomWeight(const float from, const float to){
   float random = ( (float) rand() ) / (float) RAND_MAX;
   float diff = to - from;
   float randomWeight = random * diff;
   return from + randomWeight;
}

const string Animal::getRandomName() {
   string randomName; 
   int length = ( rand()%6 ) + 4;

   char randomChar = rand()%26 + 65; 
   randomName += randomChar;

   for(int i=1;i<length;i++){
      randomChar = ( rand()%26 ) + 97;
      randomName += randomChar;
   }
   return randomName;
}

Animal* AnimalFactory::getRandomAnimal() {
   Animal* newAnimal = NULL;
   int i = rand() % 6;
   switch (i) {
      case 0: newAnimal = new Cat   ( Animal::getRandomName(), Animal::getRandomColor(), Animal::getRandomGender() ); break;
      case 1: newAnimal = new Dog   ( Animal::getRandomName(), Animal::getRandomColor(), Animal::getRandomGender() ); break;
      case 2: newAnimal = new Nunu  ( Animal::getRandomBool(), RED, Animal::getRandomGender() );                      break;
      case 3: newAnimal = new Aku   ( Animal::getRandomWeight( 5, 10 ), SILVER, Animal::getRandomGender() );          break;
      case 4: newAnimal = new Palila( Animal::getRandomName(), YELLOW, Animal::getRandomGender() );                   break;
      case 5: newAnimal = new Nene  ( Animal::getRandomName(), BROWN, Animal::getRandomGender() );                    break;
   }
   return newAnimal;
}
	
} // namespace animalfarm

///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07 - Animal Farm 4
///
/// @file node.hpp
/// @version 1.0
///
/// 
///
/// @author Joshua Lorica <loricaj@hawaii.edu>
/// @brief  Lab 07 - AnimalFarm4 - EE 205 - Spr 2021
/// @date   3/30/21
///////////////////////////////////////////////////////////////////////////////

#include "node.hpp"

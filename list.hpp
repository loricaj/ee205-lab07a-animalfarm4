///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07 - Animal Farm 4
///
/// @file list.hpp
/// @version 1.0
///
/// 
///
/// @author Joshua Lorica <loricaj@hawaii.edu>
/// @brief  Lab 07 - AnimalFarm4 - EE 205 - Spr 2021
/// @date   3/30/21
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "node.hpp"

namespace animalfarm {

class SingleLinkedList {

protected:

   Node* head = nullptr;

public:

   const bool empty() const;  // Return true if the list is empty. false if there's anything in the list
   
   void push_front( Node* newNode );   // Add newNode to the front of the list

   Node* pop_front();   // Remove a node from the front of the list. If the list is already empty, return nullptr

   Node* get_first() const;   // Return the very first node from the list. Don't make any changes to the list

   Node* get_next( const Node* currentNode ) const;   // Return the node immediately following currentNode

   unsigned int size() const;   // Return the number of nodes in the list

}; // end of SingleLinkedList class

}  // end of namespace animalfarm

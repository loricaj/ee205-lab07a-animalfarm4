///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file nene.cpp
/// @version 1.0
///
/// Exports data about all nene birds
///
/// @author Joshua Lorica <loricaj@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   2/23/21
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>

#include "nene.hpp"

using namespace std;

namespace animalfarm { 

Nene::Nene( string newString, enum Color newColor, enum Gender newGender ) {
   gender = newGender;
   species = "Branta sandvicensis";
   featherColor = newColor;
   isMigratory = true;
   tagID = newString;
}

const string Nene::speak() {
   return string( "Nay, nay" );
}

void Nene::printInfo() {
   cout << "Nene" << endl;
   cout << "   Tag ID = [" << tagID << "]" << endl;
   Bird::printInfo();
}

} // namespace animalfarm
